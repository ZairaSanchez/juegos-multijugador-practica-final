# Práctica final

Cuarta práctica de la asignatura Juegos multijugador.

## Autores
Alejandro López Soria

Marta Blanco Jaime

Pablo Molina

Zaira Sánchez

## Vídeo demostrativo

Puede verse el vídeo con la demostración del funcionamiento del proyecto [aquí](https://youtu.be/DzkdU2vM72k)

## Implementación de PUN2

Se ha implementado toda la lógica de red tanto en Mirror como en Photon Unity Networking (PUN2).
